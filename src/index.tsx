import { Elysia, NotFoundError, t } from "elysia";
import { html } from "@elysiajs/html";
import * as elements from "typed-html";

export type Todo = {
  id: number;
  content: string;
  completed: boolean;
};

function* getIdGenerator() {
  let id = 0;
  while (true) {
    yield id++;
  }
}

const idGenerator = getIdGenerator();

let todos: Todo[] = [];

const app = new Elysia()
  .use(html())
  .get("/", ({ html }) =>
    html(
      <BaseHtml>
        <body class="flex w-full overflow-hidden h-screen justify-center before:content-[''] before:h-[27%] before:fixed before:top-0 before:left-0 before:right-0 before:bg-blue-500 before:-z-[1]">
          <div hx-get="/todos" hx-swap="outerHTML" hx-trigger="load" />
        </body>
      </BaseHtml>
    )
  )
  .get("/todos", async () => {
    return <TodoList todos={todos} />;
  })
  .post(
    "/todos/toggle/:id",
    async ({ params }) => {
      const oldTodo = todos.find((todo) => todo.id === params.id);

      if (!oldTodo) {
        throw new NotFoundError();
      }

      const newTodo = {
        ...oldTodo,
        completed: !oldTodo?.completed,
      };

      todos = todos.toSpliced(
        todos.findIndex((todo) => todo.id === params.id),
        1,
        newTodo
      );

      return <TodoItem {...newTodo} />;
    },
    {
      params: t.Object({
        id: t.Numeric(),
      }),
    }
  )
  .delete(
    "/todos/:id",
    async ({ params }) => {
      todos = todos.filter((todo) => todo.id == params.id);
    },
    {
      params: t.Object({
        id: t.Numeric(),
      }),
    }
  )
  .post(
    "/todos",
    async ({ body }) => {
      const newTodo: Todo = {
        id: idGenerator.next().value as number,
        content: body.content,
        completed: false,
      };

      todos.push(newTodo);

      return <TodoItem {...newTodo} />;
    },
    {
      body: t.Object({
        content: t.String({ minLength: 1 }),
      }),
    }
  )
  .listen(3000);

console.log(
  `🦊 Elysia is running at http://${app.server?.hostname}:${app.server?.port}`
);

const BaseHtml = ({ children }: elements.Children) => `
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>THE BETH STACK</title>
  <script src="https://unpkg.com/htmx.org@1.9.3"></script>
  <script src="https://unpkg.com/hyperscript.org@0.9.9"></script>
  <script src="https://cdn.tailwindcss.com"></script>
</head>

${children}
`;

function TodoItem({ content, completed, id }: Todo) {
  return (
    <li class="py-5 px-6 flex flex-row [&:not(:last-child)]:border-b-2 border-gray-100 ">
      <input
        type="checkbox"
        checked={completed}
        hx-post={`/todos/toggle/${id}`}
        hx-swap="outerHTML"
        hx-target="closest li"
      />
      <span class="ml-auto mr-4">{content}</span>
      <button
        class="text-red-500 text-sm"
        hx-delete={`/todos/${id}`}
        hx-swap="outerHTML"
        hx-target="closest li"
      >
        delete
      </button>
    </li>
  );
}

function TodoList({ todos }: { todos: Todo[] }) {
  return (
    <div class="mt-[10%]">
      <TodoForm />
      <ul id="todos" class="bg-white rounded-lg shadow-xl mt-5">
        {todos.map((todo) => (
          <TodoItem {...todo} />
        ))}
      </ul>
    </div>
  );
}

function TodoForm() {
  return (
    <form
      hx-post="/todos"
      hx-target="#todos"
      hx-swap="beforeend"
      _="on submit target.reset()"
    >
      <input
        type="text"
        placeholder="Create a new todo"
        name="content"
        class="py-5 px-6 text-sm rounded-sm shadow-md w-[540px] focus:ring-0 focus:outline-none"
      />
    </form>
  );
}
