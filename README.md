# THE BE(T)H STACK

_It may not be the BEST stack, but it is the BE(T)H stack._

(This repo is heavily based on [this youtube video](https://youtu.be/cpzowDDJj24?si=QtX7pZX1hOphn9wS) )

## [Bun](https://bun.sh/)

## [Elysia](https://elysiajs.com/)

## ([Turso](https://turso.tech/) not used in this demo)

## [HTMX](https://htmx.org/)

Also: [typed-html](https://github.com/nicojs/typed-html), [tailwind-css](https://tailwindcss.com/)

# TO RUN LOCALLY

1. Clone this repo
2. Install [Bun](https://bun.sh)
3. Run `bun install` to install dependencies
4. Run `bun run dev` to start the dev server
